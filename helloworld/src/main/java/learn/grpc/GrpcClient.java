package learn.grpc;

import io.grpc.*;

public class GrpcClient {
    public static void main(String[] args) throws Exception {
        // Channel is the abstraction to connect to a service endpoint
        // Let's use plaintext communication because we don't have certs
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:8081")
                .usePlaintext(true)
                .build();

        // It is up to the client to determine whether to block the call
        // Here we create a blocking stub, but an async stub,
        // or an async stub with Future are always possible.
        GreetingServiceGrpc.GreetingServiceBlockingStub stub = GreetingServiceGrpc.newBlockingStub(channel);
        HelloRequest request = HelloRequest.newBuilder()
                .setFirstname("Selva")
                .setAge(10)
                .addHobbies("Cricket")
                .putBagOfTricks("throw","Oops")
                .build();

        // Finally, make the call using the stub
        for (int i = 0; i < 10; i++) {
            HelloResponse response = stub.greeting(request);
            System.out.println(response);
        }


        // A Channel should be shutdown before stopping the process.
        channel.shutdownNow();
    }
}