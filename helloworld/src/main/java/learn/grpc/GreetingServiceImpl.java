package learn.grpc;
import io.grpc.stub.StreamObserver;

public class GreetingServiceImpl extends GreetingServiceGrpc.GreetingServiceImplBase {
    @Override
    public void greeting(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        String firstname = request.getFirstname();
        HelloResponse response = HelloResponse.newBuilder()
                .setGreeting("Hello "+firstname)
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}