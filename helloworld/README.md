#### gRPC (gRPC Remote Procedure Call framework)
-- Stubby(Google's internal RPC framework - 10 power 10 RPC per second)

Simple & Idiomatic<br>
Performant & Scalable<br>
Interoperable & Extensible <br>

**Supporting technologies <br>**
1. IDL <br>
2. HTTP/2 - Transport (Binary protocol, Hpack, Streaming, multiplex) <br>
3. Protobuffer3  <br>

RunServer <br>
`$ mvn install exec:java -Dexec.mainClass=learn.grpc.MyGrpcServer` <br>
RunClient<br>
`$ mvn install exec:java -Dexec.mainClass=learn.grpc.GrpcClient`